var express = require('express'),
    dbConfig = require('../configs/database.config');

require('dotenv').config();

var app = express();

if (app.get('env') === 'development') {
    var dbConnect = dbConfig.development;
}
if (app.get('env') === 'production') {
    var dbConnect = dbConfig.production;
}

var executeNonQuery = function (query, data, callback) {
    (async () => {
        // note: we don't try/catch this because if connecting throws an exception
        // we don't need to dispose of the client (it will be undefined)
        const { Pool } = require('pg')
        const pool = new Pool(dbConnect)
        const client = await pool.connect()
        var success = 404
        var err = null

        try {
            await client.query('BEGIN')
            await client.query(query, data)
            await client.query('COMMIT')
            success = 200
        } catch (e) {
            await client.query('ROLLBACK')
            success = 404
            err = e;
            // throw e
            //res.json(e)
        } finally {
            client.release()
            pool.end()
            callback(err, success)
        }
    })().catch(e => callback(e.stack))
}

var executeScalar = function (query, data, callback) {
    (async () => {
        // note: we don't try/catch this because if connecting throws an exception
        // we don't need to dispose of the client (it will be undefined)
        const { Pool } = require('pg')
        const pool = new Pool(dbConnect)
        const client = await pool.connect()
        var result = null
        var err = null

        try {
            var queryResult = await client.query(query, data)
            result = queryResult.rows[0];
        } catch (e) {
            await client.query('ROLLBACK')
            result = 404
            err = e;
            // throw e
            //res.json(e)
        } finally {
            client.release()
            pool.end()
            callback(err, result)
        }
    })().catch(e => callback(e.stack, null))
}

var executeReader = function (query, data, callback) {
    (async () => {
        // note: we don't try/catch this because if connecting throws an exception
        // we don't need to dispose of the client (it will be undefined)
        const { Pool } = require('pg')
        const pool = new Pool(dbConnect)
        const client = await pool.connect()
        var result = null
        var err = null

        try {
            await client.query('BEGIN')
            var queryResult = await client.query(query, data)
            await client.query('COMMIT')
            result = queryResult[1].rows;
        } catch (e) {
            await client.query('ROLLBACK')
            result = 404
            err = e;
            // throw e
            //res.json(e)
        } finally {
            client.release()
            pool.end()
            callback(err, result)
        }
    })().catch(e => callback(e.stack, null))
}

module.exports = {
    executeNonQuery: executeNonQuery,
    executeScalar: executeScalar,
    executeReader: executeReader
}