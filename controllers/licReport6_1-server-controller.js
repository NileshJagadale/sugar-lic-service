var licReportSix = (function () {
    'use strict';

    var dbConnection = require('../helpers/dbconnection');

    function Report6_1Module() { }

    var _p = Report6_1Module.prototype;

    _p.InsertLicReportSix = function (req, res) {
        try {

            var data = [
                req.body.companyId,
                req.body.yearId,
                req.body.particularId,
                req.body.column1,
                req.body.column2,
                req.body.column3,
                req.body.column4,
                req.body.column5
            ];
            var query = 'SELECT "public"."insertlicreports6_1"($1,$2,$3,$4,$5,$6,$7,$8)';
            // var query = 'CALL "public"."insertlicreport6_1"($1,$2,$3,$4,$5,$6,$7,$8)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send('Data save successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.InsertBulkLicReportSix = async function (req, res) {
        try {
            for (let i = 0; i <= req.body.data.length; i++) {
                let repInfo = req.body.data[i];
                if (repInfo) {
                    var data = [
                        repInfo.companyId,
                        repInfo.yearId,
                        repInfo.particularId,
                        repInfo.column1,
                        repInfo.column2,
                        repInfo.column3,
                        repInfo.column4,
                        repInfo.column5
                    ];
                    var query = 'SELECT "public"."insertlicreports6_1"($1,$2,$3,$4,$5,$6,$7,$8)';
                    // var query = 'CALL "public"."insertlicreport6_1"($1,$2,$3,$4,$5,$6,$7,$8)';
                    await dbConnection.executeNonQuery(query, data, function (error, data) {
                        if (error) {
                            res.send('Error Occured: ' + error.message);
                            return;
                        }
                    });
                }
            }
            await res.send('Data save successfully');
        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAllLicReportSix = function (req, res) {

        try {
            var query = 'SELECT "public"."selectlicreport"(\'ref\');FETCH ALL IN "ref";';
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetLicReportSixByCompIdORYearId = function (req, res) {

        try {

            var queryData = [
                '\'' + parseInt(req.query.companyid) + '\'',
                '\'' + parseInt(req.query.yearid) + '\'',
                '\'' + 'ref' + '\''
            ];
            var query = 'SELECT "public"."selectlicreportbyid"(' + queryData[0] + ',' + queryData[1] + ',' + queryData[2] + ');' +
                'FETCH ALL IN "ref";'
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.UpdateLicReportSixByID = function (req, res) {

        try {

            var data = [
                req.body.id,
                req.body.column1,
                req.body.column2,
                req.body.column3,
                req.body.column4,
                req.body.column5
            ];
            var query = 'SELECT "public"."UpdateFactoryInformation"($1,$2,$3,$4,$5,$6)';
            dbconnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send('Update LicReport6_1 successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };


    _p.DeleteLicReportSixByID = function (req, res) {
        try {

            var data = [req.body.id];
            var query = 'SELECT "public"."DeleteFactory"($1)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send('Delete LicReport6_1 Successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    return {
        insertLicReportSix: _p.InsertLicReportSix,
        insertBulkLicReportSix: _p.InsertBulkLicReportSix,
        updateLicReportSixByID: _p.UpdateLicReportSixByID,
        getAllLicReportSix: _p.GetAllLicReportSix,
        getLicReportSixByCompIdORYearId: _p.GetLicReportSixByCompIdORYearId,
        deleteLicReportSixByID: _p.DeleteLicReportSixByID,
    };

})();

module.exports = licReportSix;