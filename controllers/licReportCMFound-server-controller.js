var cmFoundReport = (function () {
    'use strict';

    var dbConnection = require('../helpers/dbconnection');

    function CMFounds_Module() { }

    var _p = CMFounds_Module.prototype;

    _p.InsertCMFoundInfo = function (req, res) {
        try {

            var data = [
                req.body.companyId,
                req.body.yearId,
                req.body.Amount,
                req.body.bankName,
                req.body.rtgsDetails,
                req.body.challonFile
            ];
            var query = 'SELECT "public"."insert_cm_found"($1,$2,$3,$4,$5,$6)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Data save successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAllCMFound = function (req, res) {

        try {
            var query = 'SELECT "public"."select_all_cm_found"(\'ref\');FETCH ALL IN "ref";';
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetCMFoundByCompIdORYearId = function (req, res) {

        try {

            var queryData = [
                '\'' + parseInt(req.query.companyid) + '\'',
                '\'' + parseInt(req.query.yearid) + '\'',
                '\'' + 'ref' + '\''
            ];
            var query = 'SELECT "public"."select_cm_found_companyid_yearid"(' + queryData[0] + ',' + queryData[1] + ',' + queryData[2] + ');' +
                'FETCH ALL IN "ref";'
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.UpdateCMFoundByID = function (req, res) {

        try {

            var data = [
                req.body.id,
                req.body.Amount,
                req.body.bankName,
                req.body.rtgsDetails,
                req.body.challonFile
            ];
            var query = 'SELECT "public"."update_cm_found_id"($1,$2,$3,$4,$5)';
            dbconnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Update information successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };


    _p.DeleteCMFoundByID = function (req, res) {
        try {

            var data = [req.body.id];
            var query = 'SELECT "public"."delete_cm_found_id"($1)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Delete report Successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    return {
        insertCMFoundInfo: _p.InsertCMFoundInfo,
        updateCMFoundByID: _p.UpdateCMFoundByID,
        getAllCMFound: _p.GetAllCMFound,
        getCMFoundByCompIdORYearId: _p.GetCMFoundByCompIdORYearId,
        deleteCMFoundByID: _p.DeleteCMFoundByID,
    };

})();

module.exports = cmFoundReport;