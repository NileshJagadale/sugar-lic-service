var licReportAnamatRakkam = (function () {
    'use strict';

    var dbConnection = require('../helpers/dbconnection');

    function AnamatRakkam_Module() { }

    var _p = AnamatRakkam_Module.prototype;

    _p.InsertAnamatRakkam = function (req, res) {
        try {

            var data = [
                req.body.companyId,
                req.body.yearid,
                req.body.anamatRakkam,
                req.body.BankDetails,
                req.body.rtgsDetails,
                req.body.challonFile,
                req.body.outsideAreaAmount,
                req.body.outsideAmountBank,
                req.body.outsideAmountRtgsDetails,
                req.body.outsideAmountChallonFile
            ];
            var query = 'SELECT "public"."insert_anamat_rakkam"($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Data save successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAllAnamatRakkam = function (req, res) {

        try {
            var query = 'SELECT "public"."select_all_anamat_rakkam"(\'ref\');FETCH ALL IN "ref";';
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAnamatRakkamByCompIdORYearId = function (req, res) {

        try {

            var queryData = [
                '\'' + parseInt(req.query.companyid) + '\'',
                '\'' + parseInt(req.query.yearid) + '\'',
                '\'' + 'ref' + '\''
            ];
            var query = 'SELECT "public"."select_anamat_rakkam_companyid_yearid"(' + queryData[0] + ',' + queryData[1] + ',' + queryData[2] + ');' +
                'FETCH ALL IN "ref";'
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.UpdateAnamatRakkamByID = function (req, res) {

        try {

            var data = [
                req.body.id,
                req.body.anamatRakkam,
                req.body.BankDetails,
                req.body.rtgsDetails,
                req.body.challonFile,
                req.body.outsideAreaAmount,
                req.body.outsideAmountBank,
                req.body.outsideAmountRtgsDetails,
                req.body.outsideAmountChallonFile
            ];
            var query = 'SELECT "public"."update_ananat_rakkam_id"($1,$2,$3,$4,$5,$6,$7,$8,$9)';
            dbconnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Update information successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };


    _p.DeleteAnamatRakkamByID = function (req, res) {
        try {

            var data = [req.body.id];
            var query = 'SELECT "public"."delete_anamat_rakkam_id"($1)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Delete report Successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    return {
        insertAnamatRakkam: _p.InsertAnamatRakkam,
        updateAnamatRakkamByID: _p.UpdateAnamatRakkamByID,
        getAllAnamatRakkam: _p.GetAllAnamatRakkam,
        getAnamatRakkamByCompIdORYearId: _p.GetAnamatRakkamByCompIdORYearId,
        deleteAnamatRakkamByID: _p.DeleteAnamatRakkamByID,
    };

})();

module.exports = licReportAnamatRakkam;