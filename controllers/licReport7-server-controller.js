var licReportSeven = (function () {
    'use strict';

    var dbConnection = require('../helpers/dbconnection');

    function Report7_Module() { }

    var _p = Report7_Module.prototype;

    _p.InsertLicReportSeven = function (req, res) {
        try {

            var data = [
                req.body.companyId,
                req.body.yearid,
                req.body.particularid,
                req.body.labh,
                req.body.fedDate,
                req.body.fedPrincipal,
                req.body.fedInterest,
                req.body.fedTotal,
                req.body.bakiPrincipal,
                req.body.bakiInterest,
                req.body.bakiTotal,
                req.body.yearEndingPrincipal,
                req.body.yearEndingInterest,
                req.body.submissionDetails
            ];
            var query = 'SELECT "public"."insertlicreport7"($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send('Data save successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.InsertBulkLicReportSeven = async function (req, res) {
        try {
            for (let i = 0; i <= req.body.data.length; i++) {
                let repInfo = req.body.data[i];
                if (repInfo) {
                    var data = [
                        repInfo.companyId,
                        repInfo.yearid,
                        repInfo.particularid,
                        repInfo.labh,
                        repInfo.fedDate,
                        repInfo.fedPrincipal,
                        repInfo.fedInterest,
                        repInfo.fedTotal,
                        repInfo.bakiPrincipal,
                        repInfo.bakiInterest,
                        repInfo.bakiTotal,
                        repInfo.yearEndingPrincipal,
                        repInfo.yearEndingInterest,
                        repInfo.submissionDetails
                    ];
                    var query = 'SELECT "public"."insert_bulk_licreport7"($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)';
                    await dbConnection.executeNonQuery(query, data, function (error, data) {
                        if (error) {
                            res.send('Error Occured: ' + error.message);
                            return;
                        }
                    });
                }
            }
            await res.send('Data save successfully');
        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAllLicReportSeven = function (req, res) {

        try {
            var query = 'SELECT "public"."selectlicreport7"(\'ref\');FETCH ALL IN "ref";';
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetLicReportSevenByCompIdORYearId = function (req, res) {

        try {

            var queryData = [
                '\'' + parseInt(req.query.companyid) + '\'',
                '\'' + parseInt(req.query.yearid) + '\'',
                '\'' + 'ref' + '\''
            ];
            var query = 'SELECT "public"."selectlicreport7_companyid_yearid"(' + queryData[0] + ',' + queryData[1] + ',' + queryData[2] + ');' +
                'FETCH ALL IN "ref";'
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.UpdateLicReportSevenByID = function (req, res) {

        try {

            var data = [
                req.body.id,
                req.body.labh,
                req.body.fedDate,
                req.body.fedPrincipal,
                req.body.fedInterest,
                req.body.fedTotal,
                req.body.bakiPrincipal,
                req.body.bakiInterest,
                req.body.bakiTotal,
                req.body.yearEndingPrincipal,
                req.body.yearEndingInterest,
                req.body.submissionDetails
            ];
            var query = 'SELECT "public"."updatelicReport7_id"($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)';
            dbconnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send('Update LicReport6_1 successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };


    _p.DeleteLicReportSevenByID = function (req, res) {
        try {

            var data = [req.body.id];
            var query = 'SELECT "public"."deletereport7_id"($1)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error Occured: ' + error.message);
                    return;
                }
                res.send('Delete LicReport7 Successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    return {
        insertLicReportSeven: _p.InsertLicReportSeven,
        insertBulkLicReportSeven: _p.InsertBulkLicReportSeven,
        updateLicReportSevenByID: _p.UpdateLicReportSevenByID,
        getAllLicReportSeven: _p.GetAllLicReportSeven,
        getLicReportSevenByCompIdORYearId: _p.GetLicReportSevenByCompIdORYearId,
        deleteLicReportSevenByID: _p.DeleteLicReportSevenByID,
    };

})();

module.exports = licReportSeven;