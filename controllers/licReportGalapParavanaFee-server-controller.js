var galapParvanaFeesReport = (function () {
    'use strict';

    var dbConnection = require('../helpers/dbconnection');

    function galapParvanaFees_Module() { }

    var _p = galapParvanaFees_Module.prototype;

    _p.InsertGalapParvanaFeeInfo = function (req, res) {
        try {

            var data = [
                req.body.companyId,
                req.body.yearid,
                req.body.parvanafee,
                req.body.BankDetails,
                req.body.rtgsDetails,
                req.body.challonFile,
                req.body.AreaAmount,
                req.body.AreaAmountBank,
                req.body.AreaAmountRtgsDetails,
                req.body.AreaAmountChallonFile
            ];
            var query = 'SELECT "public"."insert_galap_parvana"($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Data save successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAllGalapParvanaFees = function (req, res) {

        try {
            var query = 'SELECT "public"."select_all_parvana_fees"(\'ref\');FETCH ALL IN "ref";';
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetGalapFeesByCompIdORYearId = function (req, res) {

        try {

            var queryData = [
                '\'' + parseInt(req.query.companyid) + '\'',
                '\'' + parseInt(req.query.yearid) + '\'',
                '\'' + 'ref' + '\''
            ];
            var query = 'SELECT "public"."select_galap_fees_companyid_yearid"(' + queryData[0] + ',' + queryData[1] + ',' + queryData[2] + ');' +
                'FETCH ALL IN "ref";'
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.UpdateGalapFeeByID = function (req, res) {

        try {

            var data = [
                req.body.id,
                req.body.parvanafee,
                req.body.BankDetails,
                req.body.rtgsDetails,
                req.body.challonFile,
                req.body.AreaAmount,
                req.body.AreaAmountBank,
                req.body.AreaAmountRtgsDetails,
                req.body.AreaAmountChallonFile
            ];
            var query = 'SELECT "public"."update_galap_fee_id"($1,$2,$3,$4,$5,$6,$7,$8,$9)';
            dbconnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Update information successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };


    _p.DeleteGalapFeeByID = function (req, res) {
        try {

            var data = [req.body.id];
            var query = 'SELECT "public"."delete_galap_fee_id"($1)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Delete report Successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    return {
        insertGalapParvanaFeeInfo: _p.InsertGalapParvanaFeeInfo,
        updateGalapFeeByID: _p.UpdateGalapFeeByID,
        getAllGalapParvanaFees: _p.GetAllGalapParvanaFees,
        getGalapFeesByCompIdORYearId: _p.GetGalapFeesByCompIdORYearId,
        deleteGalapFeeByID: _p.DeleteGalapFeeByID,
    };

})();

module.exports = galapParvanaFeesReport;