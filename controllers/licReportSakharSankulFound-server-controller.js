var sakharSankulFoundReport = (function () {
    'use strict';

    var dbConnection = require('../helpers/dbconnection');

    function sakharSankulFound_Module() { }

    var _p = sakharSankulFound_Module.prototype;

    _p.InsertSakharSankulFoundInfo = function (req, res) {
        try {

            var data = [
                req.body.companyId,
                req.body.yearId,
                req.body.Amount,
                req.body.bankName,
                req.body.rtgsDetails,
                req.body.challonFile
            ];
            var query = 'SELECT "public"."insert_sakhar_sankul_fee"($1,$2,$3,$4,$5,$6)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Data save successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetAllSakharSankulFounds = function (req, res) {

        try {
            var query = 'SELECT "public"."select_all_sakhar_sankul_found"(\'ref\');FETCH ALL IN "ref";';
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.GetSakharSankulFoundByCompIdORYearId = function (req, res) {

        try {

            var queryData = [
                '\'' + parseInt(req.query.companyid) + '\'',
                '\'' + parseInt(req.query.yearid) + '\'',
                '\'' + 'ref' + '\''
            ];
            var query = 'SELECT "public"."select_sakhar_sankul_founds_companyid_yearid"(' + queryData[0] + ',' + queryData[1] + ',' + queryData[2] + ');' +
                'FETCH ALL IN "ref";'
            dbConnection.executeReader(query, [], function (error, result) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send(result);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    _p.UpdateSakharSankulFoundByID = function (req, res) {

        try {

            var data = [
                req.body.id,
                req.body.Amount,
                req.body.bankName,
                req.body.rtgsDetails,
                req.body.challonFile
            ];
            var query = 'SELECT "public"."update_sakhar_sankul_found_id"($1,$2,$3,$4,$5)';
            dbconnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Update information successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };


    _p.DeleteSakharSankulFoundByID = function (req, res) {
        try {

            var data = [req.body.id];
            var query = 'SELECT "public"."delete_sakhar_sankul_found_id"($1)';
            dbConnection.executeNonQuery(query, data, function (error, data) {
                if (error) {
                    res.send('Error: ' + error.message);
                    return;
                }
                res.send('Delete report Successfully' + '\n' + data);
            });

        } catch (err) {
            res.send(err.message);
        }
    };

    return {
        insertSakharSankulFoundInfo: _p.InsertSakharSankulFoundInfo,
        updateSakharSankulFoundByID: _p.UpdateSakharSankulFoundByID,
        getAllSakharSankulFounds: _p.GetAllSakharSankulFounds,
        getSakharSankulFoundByCompIdORYearId: _p.GetSakharSankulFoundByCompIdORYearId,
        deleteSakharSankulFoundByID: _p.DeleteSakharSankulFoundByID,
    };

})();

module.exports = sakharSankulFoundReport;