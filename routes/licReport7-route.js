var report7_Route = (function () {

    'use strict';

    var express = require('express'),
        report7_Controller = require('../controllers/licReport7-server-controller'),
        report7_Router = express.Router();

    report7_Router.route('/ptrak/form7')
        .post(report7_Controller.insertLicReportSeven)
        .put(report7_Controller.updateLicReportSevenByID)
        .patch(report7_Controller.deleteLicReportSevenByID)
        .get(report7_Controller.getLicReportSevenByCompIdORYearId);

    report7_Router.route('/ptrak/all-form7')
        .get(report7_Controller.getAllLicReportSeven)
        .post(report7_Controller.insertBulkLicReportSeven);


    return report7_Router;
})();

module.exports = report7_Route;