var cmFound_Route = (function () {

    'use strict';

    var express = require('express'),
        cmFound_Controller = require('../controllers/licReportCMFound-server-controller'),
        cmFound_Router = express.Router();

    cmFound_Router.route('/ptrak/cm-found')
        .post(cmFound_Controller.insertCMFoundInfo)
        .put(cmFound_Controller.updateCMFoundByID)
        .patch(cmFound_Controller.deleteCMFoundByID)
        .get(cmFound_Controller.getCMFoundByCompIdORYearId);

    cmFound_Router.route('/ptrak/all-cm-found')
        .get(cmFound_Controller.getAllCMFound)


    return cmFound_Router;
})();

module.exports = cmFound_Route;