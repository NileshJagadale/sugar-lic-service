var sakharSankulFound_Route = (function () {

    'use strict';

    var express = require('express'),
        sakharSankulFound_Controller = require('../controllers/licReportSakharSankulFound-server-controller'),
        sakharSankulFound_Router = express.Router();

    sakharSankulFound_Router.route('/ptrak/sakhar-sankul-found')
        .post(sakharSankulFound_Controller.insertSakharSankulFoundInfo)
        .put(sakharSankulFound_Controller.updateSakharSankulFoundByID)
        .patch(sakharSankulFound_Controller.deleteSakharSankulFoundByID)
        .get(sakharSankulFound_Controller.getSakharSankulFoundByCompIdORYearId);

    sakharSankulFound_Router.route('/ptrak/all-sakhar-sankul-found')
        .get(sakharSankulFound_Controller.getAllSakharSankulFounds)


    return sakharSankulFound_Router;
})();

module.exports = sakharSankulFound_Route;