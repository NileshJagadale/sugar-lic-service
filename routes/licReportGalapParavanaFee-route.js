var galapParvanaFee_Route = (function () {

    'use strict';

    var express = require('express'),
        galapParvanaFee_Controller = require('../controllers/licReportGalapParavanaFee-server-controller'),
        galapParvanaFee_Router = express.Router();

    galapParvanaFee_Router.route('/ptrak/galap-fees')
        .post(galapParvanaFee_Controller.insertGalapParvanaFeeInfo)
        .put(galapParvanaFee_Controller.updateGalapFeeByID)
        .patch(galapParvanaFee_Controller.deleteGalapFeeByID)
        .get(galapParvanaFee_Controller.getGalapFeesByCompIdORYearId);

    galapParvanaFee_Router.route('/ptrak/all-galap-fees')
        .get(galapParvanaFee_Controller.getAllGalapParvanaFees)


    return galapParvanaFee_Router;
})();

module.exports = galapParvanaFee_Route;