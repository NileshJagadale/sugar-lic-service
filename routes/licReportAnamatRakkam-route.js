var anamatRakkam_Route = (function () {

    'use strict';

    var express = require('express'),
        anamatRakkam_Controller = require('../controllers/licReportAnamatRakkam-server-controller'),
        anamatRakkam_Router = express.Router();

    anamatRakkam_Router.route('/ptrak/anamat-rakkam')
        .post(anamatRakkam_Controller.insertAnamatRakkam)
        .put(anamatRakkam_Controller.updateAnamatRakkamByID)
        .patch(anamatRakkam_Controller.deleteAnamatRakkamByID)
        .get(anamatRakkam_Controller.getAnamatRakkamByCompIdORYearId);

    anamatRakkam_Router.route('/ptrak/all-anamat-rakkam')
        .get(anamatRakkam_Controller.getAllAnamatRakkam)


    return anamatRakkam_Router;
})();

module.exports = anamatRakkam_Route;