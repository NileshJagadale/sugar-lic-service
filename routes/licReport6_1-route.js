var report6_1Route = (function () {

    'use strict';

    var express = require('express'),
        report6_1Controller = require('../controllers/licReport6_1-server-controller'),
        report6_1Router = express.Router();

    report6_1Router.route('/ptrak/form61')
        .post(report6_1Controller.insertLicReportSix)
        .put(report6_1Controller.updateLicReportSixByID)
        .patch(report6_1Controller.deleteLicReportSixByID)
        .get(report6_1Controller.getLicReportSixByCompIdORYearId);

    report6_1Router.route('/ptrak/all-form61')
        .get(report6_1Controller.getAllLicReportSix)
        .post(report6_1Controller.insertBulkLicReportSix);


    return report6_1Router;
})();

module.exports = report6_1Route;