(function (applicationRoutes) {

    'use strict';

    applicationRoutes.init = function (app) {
        /**LIC Report 6 1 */
        var report6_1Routes = require('./licReport6_1-route');
        app.use('/api/', report6_1Routes);

        /**LIC Report 7 */
        var report7Routes = require('./licReport7-route');
        app.use('/api/', report7Routes);

        /**LIC Report Anamat Rakkam */
        var licReportAnamatRakkamRoutes = require('./licReportAnamatRakkam-route');
        app.use('/api/', licReportAnamatRakkamRoutes);

        /**LIC Report CM Found */
        var licReportCMFoundRoutes = require('./licReportCMFound-route');
        app.use('/api/', licReportCMFoundRoutes);

        /**LIC Report Sakhar Sankul Found */
        var licReportSakharSankulFoundRoutes = require('./licReportSakharSankulFound-route');
        app.use('/api/', licReportSakharSankulFoundRoutes);

        /**LIC Report Galap Paravana Fee */
        var licReportGalapParavanaFeeRoutes = require('./licReportGalapParavanaFee-route');
        app.use('/api/', licReportGalapParavanaFeeRoutes);

    }

})(module.exports);
