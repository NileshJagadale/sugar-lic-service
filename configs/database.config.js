(function () {

    'use strict';

    module.exports = {
        development: {
            host: 'localhost',
            database: 'MyFirstDB',
            user: 'postgres',
            password: 'nil@1449',
            port: 5432
        },
        production: {
            host: '199.247.15.154',
            database: 'SugarLic',
            user: 'postgres',
            password: 'StrongAdminP@ssw0rd',
            port: 5432
        }
    }
})();
