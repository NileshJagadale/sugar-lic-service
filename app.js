'use strict';

var express = require('express'),
  cors = require('cors'),
  helmet = require('helmet'),
  bodyParser = require('body-parser'),
  expressValidator = require('express-validator');

// Config environment variable.
require('dotenv').config();

// Initialize our express app
var app = express();

// helmet 
app.use(helmet())
app.use(helmet.permittedCrossDomainPolicies())
app.use(helmet.referrerPolicy({ policy: 'same-origin' }))

app.set('rootDir', __dirname);

app.use(cors());
app.use('/public', express.static('public'));
app.use(expressValidator());
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Imports routes for the low
var router = require('./routes/index.js');
router.init(app);

if (app.get('env') === 'development') {
  console.log(process.env.NODE_ENV);
  app.use(function (err, req, res, next) {
    res.header('Access-Control-Allow-Credentials', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    if (err) {
      errorLogController.postErrorLogs(err, req, next);
    }
    res.status(500);
    res.json({
      message: err
    });
  });
}


if (app.get('env') === 'production') {
  console.log(process.env.NODE_ENV);
  app.use(function (err, req, res, next) {
    res.header('Access-Control-Allow-Credentials', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if (err) {
      errorLogController.postErrorLogs(err, req, next);
    }
    res.status(500);
    res.json({
      message: err
    });
  });
}

const port = process.env.PORT;

app.listen(port, () => {
  console.log('Server is started on port number ' + port);
});

module.exports = app; 
